﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_PAIN
{
    public class FileViewModelCreatedEventArgs : EventArgs
    {
        public FileViewModel Instance
        { get; set; }
    }

    public class ErrorOcurredEventArgs : EventArgs
    {
        public string Message
        { get; set; }
    }
}
