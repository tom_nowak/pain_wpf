﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_PAIN
{
    public class FileViewModel
    {
        public FileViewModel(string path)
        {
            this.fullPath = path;
            contents = FilesystemModel.ReadFile(fullPath);
        }

        private string fullPath;
        public string FullPath
        {
            get
            {
                return fullPath;
            }
        }

        public string contents;
        public string Contents
        {
            get
            {
                return contents;
            }
        }
    }
}
