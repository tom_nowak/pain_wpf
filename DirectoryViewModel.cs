﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_PAIN
{
    public class DirectoryViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, args) => { };
        public event EventHandler<FileViewModelCreatedEventArgs> FileViewModelCreated = (sender, args) => { };
        public event EventHandler<ErrorOcurredEventArgs> ErrorOcurred = (sender, args) => { };

        public DirectoryViewModel()
        {
            FullPath = FilesystemModel.GetDefaultDrive();
        }

        private OpenSubdirectoryCommand openSubdirectoryCommand = new OpenSubdirectoryCommand();
        public OpenSubdirectoryCommand OpenSubdirectory
        {
            get
            {
                return openSubdirectoryCommand;
            }
        }

        private OpenFileCommand openFileCommand = new OpenFileCommand();
        public OpenFileCommand OpenFile
        {
            get
            {
                return openFileCommand;
            }
        }
  
        private string fullPath;
        public string FullPath
        {
            get
            {
                return fullPath;
            }
            set
            {
                if (fullPath == value)
                    return;
                try
                {
                    var newSubdirectories = GetSubdirectoriesNames(value);
                    var newFiles = FilesystemModel.GetFiles(value).Select(FilesystemModel.GetFileOrDirectoryName).ToList();
                    fullPath = value;
                    subdirectories = newSubdirectories;
                    files = newFiles;
                    PropertyChanged(this, new PropertyChangedEventArgs("FullPath"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Subdirectories"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Files"));
                }
                catch
                {
                    ErrorOcurred(this, new ErrorOcurredEventArgs() { Message = "Failed to open " + value });
                }
            }
        }

        private List<string> subdirectories = new List<string>();
        public List<string> Subdirectories
        {
            get
            {
                return subdirectories;
            }
        }

        private List<string> files = new List<string>();
        public List<string> Files
        {
            get
            {
                return files;
            }
        }

        private int selectedDirectoryIndex = -1;
        public int SelectedDirectoryIndex
        {
            get
            {
                return selectedDirectoryIndex;
            }
            set
            {
                if (value == selectedDirectoryIndex)
                    return;
                selectedDirectoryIndex = value;
                openSubdirectoryCommand.EmitCanExecuteChanged();
            }
        }

        private int selectedFileIndex = -1;
        public int SelectedFileIndex
        {
            get
            {
                return selectedFileIndex;
            }
            set
            {
                if (value == selectedFileIndex)
                    return;
                selectedFileIndex = value;
                openFileCommand.EmitCanExecuteChanged();
            }

        }

        public void SpawnNewFileViewModel(string fullFilePath)
        {
            try
            {
                var newViewModel = new FileViewModel(fullFilePath);
                FileViewModelCreated(this, new FileViewModelCreatedEventArgs() { Instance = newViewModel });
            }
            catch
            {
                ErrorOcurred(this, new ErrorOcurredEventArgs() { Message = "Failed to open " + fullFilePath });
            }
        }

        private static List<string> GetSubdirectoriesNames(string newPath)
        {
            var subdirectoriesFullPaths = FilesystemModel.GetSubdirectories(newPath);
            var newSubdirectories = new List<string>();
            if (!FilesystemModel.IsRootDirectory(newPath))
                newSubdirectories.Add("..");
            newSubdirectories.AddRange(subdirectoriesFullPaths.Select(FilesystemModel.GetFileOrDirectoryName));
            return newSubdirectories;
        }
    }
}
