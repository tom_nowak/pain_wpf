﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_PAIN
{
    public static class FilesystemModel
    {
        public static string GetDefaultDrive()
        {
            return Directory.GetLogicalDrives()[1];
        }

        public static List<String> GetSubdirectories(string directoryPath)
        {
            return new List<String>(Directory.GetDirectories(directoryPath));
        }

        public static List<String> GetFiles(string directoryPath)
        {
            return new List<string>(Directory.GetFiles(directoryPath));
        }

        public static bool IsRootDirectory(string path)
        {
            return Directory.GetLogicalDrives().Contains(path);
        }

        public static bool IsTextFile(string filename)
        {
            return filename.EndsWith(".txt") || filename.EndsWith(".cs");
        }

        public static string GetParentDirectory(string directoryPath)
        {
            if (IsRootDirectory(directoryPath))
                return directoryPath;
            return Directory.GetParent(directoryPath).ToString();
        }

        public static string CombinePath(string directoryPath, string fileOrDirectory)
        {
            return directoryPath + '\\' + fileOrDirectory;
        }

        public static string GetFileOrDirectoryName(string fullPath)
        {
            var lastIndex = fullPath.LastIndexOf('\\');
            if (lastIndex <= 0)
                return fullPath;
            return fullPath.Substring(lastIndex + 1);
        }

        public static string ReadFile(string fullPath)
        {
            return File.ReadAllText(fullPath);
        }
    }
}
