﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPF_PAIN
{
    public class OpenSubdirectoryCommand : ICommand
    {
        public event EventHandler CanExecuteChanged = (sender, args) => { };

        public bool CanExecute(object parameter)
        {
            var viewModel = (DirectoryViewModel)parameter;
            return viewModel.SelectedDirectoryIndex >= 0 &&
                viewModel.SelectedDirectoryIndex < viewModel.Subdirectories.Count;
        }

        public void Execute(object parameter)
        {
            var viewModel = (DirectoryViewModel)parameter;
            string selectedName = viewModel.Subdirectories[viewModel.SelectedDirectoryIndex];
            if (selectedName == "..")
                viewModel.FullPath = FilesystemModel.GetParentDirectory(viewModel.FullPath);
            else
                viewModel.FullPath = FilesystemModel.CombinePath(viewModel.FullPath, selectedName);
        }

        public void EmitCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }
    }

    public class OpenFileCommand : ICommand
    {
        public event EventHandler CanExecuteChanged = (sender, args) => { };

        public bool CanExecute(object parameter)
        {
            var viewModel = (DirectoryViewModel)parameter;
            if (viewModel.SelectedFileIndex < 0 || viewModel.SelectedFileIndex >= viewModel.Files.Count)
                return false;
            string selectedName = viewModel.Files[viewModel.SelectedFileIndex];
            return FilesystemModel.IsTextFile(selectedName);
        }

        public void Execute(object parameter)
        {
            var viewModel = (DirectoryViewModel)parameter;
            string selectedName = viewModel.Files[viewModel.SelectedFileIndex];
            string filePath = FilesystemModel.CombinePath(viewModel.FullPath, selectedName);
            viewModel.SpawnNewFileViewModel(filePath);
        }
 
        public void EmitCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
