﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_PAIN
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DirectoryViewModel viewModel = new DirectoryViewModel(); 

        public MainWindow()
        {
            InitializeComponent();
            DataContext = viewModel;
            viewModel.FileViewModelCreated += (sender, e) => new TextFileContents() { DataContext = e.Instance }.Show();
            viewModel.ErrorOcurred += (sender, e) => MessageBox.Show(this, e.Message, "Error");
        }
    }
}
